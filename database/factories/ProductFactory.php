<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'description' => $faker->text(150),
        'price' => rand(100, 500),
        'quantity' => rand(20, 100),
        'category_id' => rand(1, 200),
        'user_id' => 1,
    ];
});