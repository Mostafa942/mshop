<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'level' => 1,
        'description' => $faker->text(150),
        'parent_id' => rand(1, 2),
    ];
});
