<a href="{{ route('admin.client.show', $model->id) }}" class="btn btn-outline-info">Show</a>
<button type="button" class="btn btn-outline-danger delete"
    data-url="{{ route('admin.client.destroy', $model->id) }}">Delete</button>
