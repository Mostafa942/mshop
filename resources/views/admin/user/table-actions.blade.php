<a href="{{ route('admin.user.show', $model->id) }}" class="btn btn-outline-info">Show</a>
<button type="button" class="btn btn-outline-danger delete"
    data-url="{{ route('admin.user.destroy', $model->id) }}">Delete</button>
