@extends('layouts.app')

@section('title', 'Show Category')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Show Category</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Admin</a></li>
                        <li class="breadcrumb-item active"><a href="{{ route('admin.category.index') }}">Category
                                Page</a></li>
                        <li class="breadcrumb-item active">Show Category</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="card card-body">
                    <h5 class="card-header">{{ $category->name }}</h5>
                    <div class="card-body">
                        @if ($category->parent)
                        <h5 class="card-title">Parent : <a
                                href="{{ route('admin.category.show', $category->parent) }}">{{ $category->parent->name }}</a>
                        </h5>
                        @endif
                        <p class="card-text">Description : {{ $category->description }}</p>
                        <p class="card-text">Created at : {{ $category->created_at }}</p>
                        @if (auth()->user()->is_admin)
                        <a href="{{ route('admin.category.edit', $category) }}" class="btn btn-primary">Edit</a>
                        @endif
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
