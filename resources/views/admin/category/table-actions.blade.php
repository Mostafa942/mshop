<a href="{{ route('admin.category.show', $model->id) }}" class="btn btn-outline-info">Show</a>
@if (auth()->user()->is_admin)
<a href="{{ route('admin.category.edit', $model->id) }}" class="btn btn-outline-dark">Edit</a>
<button type="button" class="btn btn-outline-danger delete"
    data-url="{{ route('admin.category.destroy', $model->id) }}">Delete</button>
@endif
