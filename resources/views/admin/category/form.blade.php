@csrf
<div class="form-group">
    <label for="parent_id">Parent Category</label>
    <select name="parent_id" id="parent_id" class="form-control">
        <option value="">Not Set</option>
        @foreach ($categories as $rowCategory)
        @if (isset($category) && $category->parent && $rowCategory->id == $category->parent->id)
        <option value="{{ $rowCategory->id }}" selected>{{ $rowCategory->name }}</option>
        @else
        <option value="{{ $rowCategory->id }}">{{ $rowCategory->name }}</option>
        @endif
        @endforeach
        @error('parent_id')
        <span class="invalid-feedback" role="alert">
            <strong class="text-danger">{{ $message }}</strong>
        </span>
        @enderror
    </select>
</div>
<div class="form-group">
    <label for="name">Name</label>
    <input type="text" name="name" class="form-control" value="{{ isset($category) ? $category->name : '' }}">
    @error('name')
    <span class="invalid-feedback" role="alert">
        <strong class="text-danger">{{ $message }}</strong>
    </span>
    @enderror
</div>
<div class="form-group">
    <label for="description">Description</label>
    <textarea name="description" id="description" cols="30" rows="10"
        class="form-control">{{ isset($category) ? $category->description : '' }}</textarea>
    @error('description')
    <span class="invalid-feedback">
        <strong class="text-danger">{{ $message }}</strong>
    </span>
    @enderror
</div>
