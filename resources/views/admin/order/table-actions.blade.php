<a href="{{ route('admin.order.show', $model->id) }}" class="btn btn-outline-info">Show</a>
@if (auth()->user()->is_admin)
<a href="{{ route('admin.order.edit', $model->id) }}" class="btn btn-outline-dark">Edit</a>
<button type="button" class="btn btn-outline-danger delete"
    data-url="{{ route('admin.order.destroy', $model->id) }}">Delete</button>
@endif
