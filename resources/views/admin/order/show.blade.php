@extends('layouts.app')

@section('title', 'Show Order')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Show Order</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Admin</a></li>
                            <li class="breadcrumb-item active"><a href="{{ route('admin.order.index') }}">Order
                                    Page</a></li>
                            <li class="breadcrumb-item active">Show Order</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="card card-body">
                        <h2>Client Name : {{ $order->client->name }}</h2>
                        <h2>User Name : {{ $order->user->name }}</h2>
                        <div class="overflow-auto">
                            <table class="table table-bordered text-center">
                                <thead class="bg-dark text-light">
                                    <tr>
                                        <th>Product Name</th>
                                        <th>Price ($)</th>
                                        <th>quantity</th>
                                        <th>Total Price ($)</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                @foreach ($order->products as $product)
                                    <tbody>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->price }}</td>
                                        <td>{{ $product->pivot->quantity }}</td>
                                        <td>{{ $product->pivot->total_price }}</td>
                                        @if ($product->deleted_at)
                                            <td>{{ $product->name }} has been deleted</td>
                                        @else
                                            <td><a class="btn btn-outline-info"
                                                    href="{{ route('admin.product.show', $product->id) }}">Show Product</a>
                                            </td>
                                        @endif
                                    </tbody>
                                @endforeach
                            </table>
                        </div>
                        <h1 class="mt-2">Total order Price : {{ $order->total }} $</h1>
                    </div>
                </div>
                <a href="{{ route('admin.order.edit', $order) }}" class="btn btn-outline-dark">Edit</a>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
