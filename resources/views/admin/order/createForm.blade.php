@extends('layouts.app')

@section('title', 'Create New Order')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Create Order</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Admin</a></li>
                            <li class="breadcrumb-item active"><a href="{{ route('admin.order.index') }}">Order
                                    Page</a></li>
                            <li class="breadcrumb-item active">Create Order</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="card card-body">
                        <form action="{{ route('admin.order.store') }}" method="POST">
                            @csrf
                            <div class="row">
                                <div class="col-6">
                                    <label for="client_id">Client</label>
                                    <select name="client_id" id="client_id" class="form-control">
                                        <option value="">Not Set</option>
                                        @foreach ($clients as $client)
                                            <option value="{{ $client->id }}"
                                                {{ old($client->id) == $client->id ? 'selected' : '' }}>{{ $client->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @error('client_id')
                                    <span class="invalid-feedback">
                                        <strong class="text-danger">{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-6">
                                    <label for="date">Date</label>
                                    <input type="date" name="date" id="date" class="form-control" value="{{ old('date') }}">
                                    @error('date')
                                    <span class="invalid-feedback">
                                        <strong class="text-danger">{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row pt-2 pb-2">
                                <div class="col">
                                    <h3>Products</h3>
                                </div>
                                <div class="col">
                                    @error('products')
                                    <span class="invalid-feedback">
                                        <strong class="text-danger">{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    @error('products.duplicate')
                                    <span class="invalid-feedback">
                                        <strong class="text-danger">{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    @error('invalid.quantity')
                                    <span class="invalid-feedback">
                                        <strong class="text-danger">{{ $message }}</strong>
                                    </span>
                                    @enderror

                                    {{-- @if ($errors->any())
                                        @dd($errors);
                                    @endif --}}

                                </div>
                                <button type="button" class="btn btn-info" id="btn-add-product-row">Add Product</button>
                            </div>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>Quantity</th>
                                        <th>Price</th>
                                        <th>Total_price</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody id="product-list">
                                    @if (old('products'))
                                        @foreach (old('products') as $rowId => $rowProduct)
                                            <tr id="product-{{ $rowId }}">
                                                <td>
                                                    <select name="products[{{ $rowId }}][product_id]"
                                                        class="form-control input-product-product_id"
                                                        row-id="product-{{ $rowId }}">
                                                        @foreach ($products as $product)
                                                            @if ($product->id == $rowProduct['product_id'])
                                                                <option value="{{ $product->id }}"
                                                                    data-price="{{ $product->price }}" selected>
                                                                    {{ $product->name }} | {{ $product->price }}
                                                                </option>
                                                            @else
                                                                <option value="{{ $product->id }}"
                                                                    data-price="{{ $product->price }}">
                                                                    {{ $product->name }} | {{ $product->price }}
                                                                </option>
                                                            @endif

                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td>
                                                    <input type="text" name="products[{{ $rowId }}][quantity]"
                                                        value="{{ $rowProduct['quantity'] ?? 1 }}"
                                                        row-id="product-{{ $rowId }}"
                                                        class="form-control input-product-quantity">
                                                    @error('products.' . $rowId . '.quantity')
                                                    <span class="invalid-feedback">
                                                        <strong class="text-danger">{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </td>
                                                <td>
                                                    <input type="number" name="products[{{ $rowId }}][price]"
                                                        value="{{ $rowProduct['price'] ?? $products->price }}"
                                                        class="form-control input-product-price"
                                                        row-id="product-{{ $rowId }}" readonly>
                                                    @error('products.' . $rowId . '.price')
                                                    <span class="invalid-feedback">
                                                        <strong class="text-danger">{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </td>
                                                <td>
                                                    <input type="number" name="products[{{ $rowId }}][total_price]"
                                                        value="{{ $rowProduct['total_price'] ?? old($rowProduct['total_price']) }}"
                                                        class="form-control input-product-total_price"
                                                        row-id="product-{{ $rowId }}" readonly>
                                                    @error('products.' . $rowId . '.total_price')
                                                    <span class="invalid-feedback">
                                                        <strong class="text-danger">{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-danger btn-delete"
                                                        row-id="product-{{ $rowId }}">Remove</button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr id="product-1">
                                            <td>
                                                <select name="products[1][product_id]"
                                                    class="form-control input-product-product_id" row-id="product-1">
                                                    @foreach ($products as $product)
                                                        <option value="{{ $product->id }}"
                                                            data-price="{{ $product->price }}">
                                                            {{ $product->name }} | {{ $product->price }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" name="products[1][quantity]" row-id="product-1"
                                                    class="form-control input-product-quantity">
                                            </td>
                                            <td>
                                                <input type="number" name="products[1][price]"
                                                    value="{{ $products->first()->price }}"
                                                    class="form-control input-product-price" row-id="product-1" readonly>
                                            </td>
                                            <td>
                                                <input type="number" name="products[1][total_price]"
                                                    class="form-control input-product-total_price" row-id="product-1"
                                                    readonly>
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-danger btn-delete"
                                                    row-id="product-1">Remove</button>
                                            </td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                            <label for="total">Total Order Price</label>
                            <input type="text" name="total" class="total form-control" value="{{ old('total') }}" readonly>
                            <button type="submit" class="btn btn-success mt-4">Save</button>
                        </form>
                    </div>
                </div>
                <!-- /.row -->

            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('js')
<script>
    $(document).on('click', '#btn-add-product-row', function() {
        const rowId = Date.now(),
            productRow = `
        <tr id="product-${rowId}">
            <td>
                <select name="products[${rowId}][product_id]" id="product" class="form-control input-product-product_id" row-id="product-${rowId}">
                    @foreach ($products as $product)
                    <option value="{{ $product->id }}" data-price="{{ $product->price }}">
                        {{ $product->name }} | {{ $product->price }}
                    </option>
                    @endforeach
                </select>
            </td>
            <td>
                <input type="text"
                    name="products[${rowId}][quantity]"
                    row-id="product-${rowId}"
                    class="form-control input-product-quantity">
            </td>
            <td>
                <input type="number"
                    name="products[${rowId}][price]"
                    value="{{ $products->first()->price }}"
                    class="form-control input-product-price"
                    row-id="product-${rowId}"
                    readonly>
            </td>
            <td>
                <input type="number"
                    name="products[${rowId}][total_price]"
                    value="{{ $products->first()->price }}"
                    class="form-control input-product-total_price"
                    row-id="product-${rowId}"
                    readonly>
            </td>
            <td>
                <button type="button" class="btn btn-danger btn-delete" row-id="product-${rowId}">Remove</button>
            </td>
        </tr>
        `
        $('#product-list').append(productRow)
    });
    $(document).on('click', '.btn-delete', function() {
        const rowId = '#' + $(this).attr('row-id');
        $('.total').val($('.total').val() - $(`${rowId} .input-product-total_price`).val());
        $(rowId).remove();
    });
    $(document).on('keyup', '.input-product-quantity', function(e) {
        const quantity = e.target.value,
            rowId = '#' + $(this).attr('row-id')
        price = $(`${rowId} .input-product-price`).val(),
            totalPrice = quantity * price;
        $(`${rowId} .input-product-total_price`).val(totalPrice);
        var total = 0;
        $(".input-product-total_price").each(function() {
            quantit = parseInt($(this).val());
            if (!isNaN(quantit)) {
                total += quantit;
            }
        });
        $('.total').val(+total);
    });
    $(document).on('change', '.input-product-product_id', function() {
        const rowId = '#' + $(this).attr('row-id'),
            price = $(this).children("option:selected").data('price');
        $(`${rowId} .input-product-price`).val(price);
        $(`${rowId} .input-product-total_price`).val(price);
        quantity = $(`${rowId} .input-product-quantity`).val();
        if (quantity > 0) {
            totalPrice = quantity * price;
            $(`${rowId} .input-product-total_price`).val(totalPrice);
        }
        var total = 0;
        $(".input-product-total_price").each(function() {
            quantit = parseInt($(this).val());
            if (!isNaN(quantit)) {
                total += quantit;
            }
        });
        $('.total').val(+total);
    });
</script>
@endsection
