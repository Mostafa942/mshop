@extends('layouts.app')

@section('title', 'Edit Order')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Edit Order</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Admin</a></li>
                            <li class="breadcrumb-item active"><a href="{{ route('admin.order.index') }}">Order
                                    Page</a></li>
                            <li class="breadcrumb-item active">Edit Order</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="card card-body">
                        <form action="{{ route('admin.order.update', $order) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="col-6">
                                    <label for="client_id">Client</label>
                                    <select name="client_id" id="client_id" class="form-control">
                                        <option value="">Not Set</option>
                                        @foreach ($clients as $client)
                                            @if (isset($order) && $client->id == $order->client_id)
                                                <option value="{{ $client->id }}" selected>{{ $client->name }}</option>
                                            @else
                                                <option value="{{ $client->id }}">{{ $client->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    @error('client_id')
                                    <span class="invalid-feedback">
                                        <strong class="text-danger">{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-6">
                                    <label for="date">Date</label>

                                    <input type="date" name="date" id="date" class="form-control"
                                        value="{{ isset($order) ? $order->date : '' }}">
                                    @error('date')
                                    <span class="invalid-feedback">
                                        <strong class="text-danger">{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row pt-2 pb-2">
                                <div class="col">
                                    <h3>Products</h3>
                                </div>
                                <div class="col">
                                    @error('products')
                                    <span class="invalid-feedback">
                                        <strong class="text-danger">{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    @error('products.duplicate')
                                    <span class="invalid-feedback">
                                        <strong class="text-danger">{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    @error('invalid.quantity')
                                    <span class="invalid-feedback">
                                        <strong class="text-danger">{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <button type="button" class="btn btn-info" id="btn-add-product-row">Add Product</button>
                            </div>
                            <label for="total">Total Order Price</label>
                            <input type="text" name="total" id="total" class="total form-control"
                                value="{{ isset($order) ? $order->total : '' }}" readonly>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>Quantity</th>
                                        <th>Price</th>
                                        <th>Total_price</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody id="product-list">
                                    @foreach ($order->products as $order)
                                        {{-- @dd($order) --}}
                                        <tr id="product-{{ $order->id }}"
                                            style=" {{ $order->deleted_at ? 'pointer-events: none; opacity: 0.5' : '' }}">
                                            <td>
                                                <select name="products[{{ $order->id }}][product_id]"
                                                    class="form-control input-product-product_id"
                                                    row-id="product-{{ $order->id }}">
                                                    @foreach ($products as $product)
                                                        @if (isset($order) && $product->id == $order->id)
                                                            <option value="{{ $product->id }}"
                                                                data-price="{{ $product->price }}" selected>
                                                                {{ $product->name }} | {{ $product->price }}
                                                            </option>
                                                        @else
                                                            <option value="{{ $product->id }}"
                                                                data-price="{{ $product->price }}">
                                                                {{ $product->name }} | {{ $product->price }}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" name="products[{{ $order->id }}][quantity]"
                                                    value="{{ $order->pivot->quantity }}" row-id="product-{{ $order->id }}"
                                                    class="form-control input-product-quantity">
                                                @error('products.' . $order->id . '.quantity')
                                                <span class="invalid-feedback">
                                                    <strong class="text-danger">{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </td>
                                            <td>
                                                <input type="number" name="products[{{ $order->id }}][price]"
                                                    value="{{ $order->price }}" class="form-control input-product-price"
                                                    row-id="product-{{ $order->id }}" readonly>
                                                @error('products.' . $order->id . '.price')
                                                <span class="invalid-feedback">
                                                    <strong class="text-danger">{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </td>
                                            <td>
                                                <input type="number" name="products[{{ $order->id }}][total_price]"
                                                    value="{{ $order->pivot->total_price }}"
                                                    class="form-control input-product-total_price"
                                                    row-id="product-{{ $order->id }}" readonly>
                                                @error('products.' . $order->id . '.total_price')
                                                <span class="invalid-feedback">
                                                    <strong class="text-danger">{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-danger btn-delete"
                                                    row-id="product-{{ $order->id }}">Remove</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <button type="submit" class="btn btn-success mt-4">Update</button>
                        </form>

                    </div>
                </div>
                <!-- /.row -->

            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('js')
    <script>
        $(document).on('click', '#btn-add-product-row', function() {
            const rowId = Date.now(),
                productRow = `
            <tr id="product-${rowId}">
                <td>
                    <select name="products[${rowId}][product_id]" id="product" class="form-control input-product-product_id" row-id="product-${rowId}">
                        @foreach ($products as $product)
                        <option value="{{ $product->id }}" data-price="{{ $product->price }}">
                            {{ $product->name }} | {{ $product->price }}
                        </option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <input type="text"
                        name="products[${rowId}][quantity]"
                        row-id="product-${rowId}"
                        class="form-control input-product-quantity">
                </td>
                <td>
                    <input type="number"
                        name="products[${rowId}][price]"
                        value="{{ $products->first()->price }}"
                        class="form-control input-product-price"
                        row-id="product-${rowId}"
                        readonly>
                </td>
                <td>
                    <input type="number"
                        name="products[${rowId}][total_price]"
                        value="{{ $products->first()->price }}"
                        class="form-control input-product-total_price"
                        row-id="product-${rowId}"
                        readonly>
                </td>
                <td>
                    <button type="button" class="btn btn-danger btn-delete" row-id="product-${rowId}">Remove</button>
                </td>
            </tr>
            `
            $('#product-list').append(productRow)
        });
        $(document).on('click', '.btn-delete', function() {
            const rowId = '#' + $(this).attr('row-id');
            $('.total').val($('.total').val() - $(`${rowId} .input-product-total_price`).val());
            $(rowId).remove();
        });
        $(document).on('keyup', '.input-product-quantity', function(e) {
            const quantity = e.target.value,
                rowId = '#' + $(this).attr('row-id')
            price = $(`${rowId} .input-product-price`).val(),
                totalPrice = quantity * price;
            $(`${rowId} .input-product-total_price`).val(totalPrice);
            var total = 0;
            $(".input-product-total_price").each(function() {
                quantit = parseInt($(this).val());
                if (!isNaN(quantit)) {
                    total += quantit;
                }
            });
            $('.total').val(+total);
        });
        $(document).on('change', '.input-product-product_id', function() {
            const rowId = '#' + $(this).attr('row-id'),
                price = $(this).children("option:selected").data('price');
            $(`${rowId} .input-product-price`).val(price);
            $(`${rowId} .input-product-total_price`).val(price);
            quantity = $(`${rowId} .input-product-quantity`).val();
            if (quantity > 0) {
                totalPrice = quantity * price;
                $(`${rowId} .input-product-total_price`).val(totalPrice);
            }
            var total = 0;
            $(".input-product-total_price").each(function() {
                quantit = parseInt($(this).val());
                if (!isNaN(quantit)) {
                    total += quantit;
                }
            });
            $('.total').val(+total);
        });

    </script>
@endsection
