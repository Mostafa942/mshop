@csrf
<div class="form-group">
    <label for="user_id">User</label>
    <select name="user_id" id="user_id" class="form-control">
        <option value="">Not Set</option>
        @foreach ($users as $user)
        @if (isset($product) && $user->id == $product->user_id)
        <option value="{{ $user->id }}" selected>{{ $user->name }}</option>
        @else
        <option value="{{ $user->id }}">{{ $user->name }}</option>
        @endif
        @endforeach
        @error('user_id')
        <span class="invalid-feedback" role="alert">
            <strong class="text-danger">{{ $message }}</strong>
        </span>
        @enderror
    </select>
</div>
<div class="form-group">
    <label for="category_id">Category</label>
    <select name="category_id" id="category_id" class="form-control">
        <option value="">Not Set</option>
        @foreach ($categories as $category)
        @if (isset($product) && $category->id == $product->category_id)
        <option value="{{ $category->id }}" selected>{{ $category->name }}</option>
        @else
        <option value="{{ $category->id }}">{{ $category->name }}</option>
        @endif
        @endforeach
        @error('category_id')
        <span class="invalid-feedback" role="alert">
            <strong class="text-danger">{{ $message }}</strong>
        </span>
        @enderror
    </select>
</div>
<div class="form-group">
    <label for="name">Name</label>
    <input type="text" name="name" class="form-control" value="{{ isset($product) ? $product->name : '' }}">
    @error('name')
    <span class="invalid-feedback" role="alert">
        <strong class="text-danger">{{ $message }}</strong>
    </span>
    @enderror
</div>
<div class="form-group">
    <label for="description">Description</label>
    <textarea name="description" id="description" cols="30" rows="10"
        class="form-control">{{ isset($product) ? $product->description : '' }}</textarea>
    @error('description')
    <span class="invalid-feedback" role="alert">
        <strong class="text-danger">{{ $message }}</strong>
    </span>
    @enderror
</div>
<div class="form-group">
    <label for="price">Price</label>
    <input type="number" name="price" class="form-control" value="{{ isset($product) ? $product->price : '' }}">
    @error('price')
    <span class="invalid-feedback" role="alert">
        <strong class="text-danger">{{ $message }}</strong>
    </span>
    @enderror
</div>
<div class="form-group">
    <label for="quantity">Quantity</label>
    <input type="number" name="quantity" class="form-control" value="{{ isset($product) ? $product->quantity : '' }}">
    @error('quantity')
    <span class="invalid-feedback" role="alert">
        <strong class="text-danger">{{ $message }}</strong>
    </span>
    @enderror
</div>
{{-- <form action="{{ route('admin.image.upload.post') }}" method="POST" enctype="multipart/form-data">
@csrf

<div class="col-md-6">
    <input type="file" name="image" class="form-control">
</div>
@if ($errors->first('image'))
<span class="text-danger">
    {{ $errors->first('image') }}
</span>
@endif
<div class="col-md-6">
    <button type="submit" class="btn btn-success d-block">Save</button>
</div>
</form> --}}
