@include('layouts.parts.css')

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        @include('layouts.parts.nav')

        @include('layouts.parts.aside')

        @yield('content')

        @include('layouts.parts.footer')
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    @include('layouts.parts.js')
    @yield('js')
</body>

</html>
