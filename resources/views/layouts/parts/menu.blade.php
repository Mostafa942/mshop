<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
    <li class="nav-item">
        <a href="{{ route('admin.dashboard') }}"
            class="nav-link {{ Route::currentRouteName() == 'admin.dashboard' ? 'active' : '' }}">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
                Dashboard
            </p>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('admin.category.index') }}"
            class="nav-link {{ Route::currentRouteName() == 'admin.category.index' ? 'active' : '' }}">
            <i class="nav-icon fas fa-list-alt"></i>
            <p>
                Categories
            </p>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('admin.product.index') }}"
            class="nav-link {{ Route::currentRouteName() == 'admin.product.index'? 'active' : '' }}">
            <i class="nav-icon fab fa-product-hunt"></i>
            <p>
                Products
            </p>
        </a>
    </li>
    @if (auth()->user()->is_admin)
    <li class="nav-item">
        <a href="{{ route('admin.order.index') }}"
            class="nav-link {{ Route::currentRouteName() == 'admin.order.index'? 'active' : '' }}">
            <i class="nav-icon fas fa-shopping-cart"></i>
            <p>
                Orders
            </p>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('admin.user.index') }}"
            class="nav-link {{ Route::currentRouteName() == 'admin.user.index'? 'active' : '' }}">
            <i class="nav-icon fas fa-users"></i>
            <p>
                Users
            </p>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('admin.client.index') }}"
            class="nav-link {{ Route::currentRouteName() == 'admin.client.index'? 'active' : '' }}">
            <i class="nav-icon fas fa-handshake"></i>
            <p>
                Clients
            </p>
        </a>
    </li>
    @endif
</ul>
