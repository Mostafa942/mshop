<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class DashboardController extends Controller {
    public function index() {
        $category = DB::table('categories')->where('deleted_at', null)->count();
        $product = DB::table('products')->where('deleted_at', null)->count();
        $order = DB::table('orders')->where('deleted_at', null)->count();
        $client = DB::table('clients')->count();
        $user = DB::table('users')->where('is_admin', 0)->count();
        return view('admin.dashboard', [
            'category' => $category,
            'product' => $product,
            'order' => $order,
            'client' => $client,
            'user' => $user
        ]);
    }
}
