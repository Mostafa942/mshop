<?php

namespace App\Http\Controllers;

use App\Client;
use App\Http\Requests\OrderRequest;
use App\Order;
use App\Product;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $total = DB::table('orders')->where('deleted_at', null)->sum('total');
        return view('admin.order.index', [
            'total' => $total,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.order.createForm', [
            'clients' => Client::get([
                'id',
                'name',
            ]),
            'products' => Product::get([
                'id',
                'name',
                'price',
            ]),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderRequest $request)
    {
        $request->merge([
            'user_id' => $request->user()->id,
        ]);
        $products = $request->products;

        $productIds = [];

        $total = 0;

        $orderDetails = [];

        foreach ($products as $product) {
            $InventoryQuantity = DB::table('products')
                ->where('id', $product['product_id'])
                ->get('quantity')->first()->quantity;

            $productIds[] = $product['product_id'];

            $productPrice = $product['price'];

            $productQuantity = $product['quantity'];

            if ($InventoryQuantity < $productQuantity) {
                $error = ['invalid.quantity' => 'The inventory quantity of product ' . $product['product_id'] . ' is less than the ordered quantity'];
                return redirect()->back()->withErrors($error)->withInput();
            }

            $productTotalPrice = $productPrice * $productQuantity;

            $total += $productTotalPrice;

            $orderDetails[] = [
                'product_id' => $product['product_id'],
                'quantity' => $productQuantity,
                'price' => $productPrice,
                'total_price' => $productTotalPrice,
            ];
        }

        if (count($productIds) != count(array_unique($productIds))) {
            $error = ['products.duplicate' => 'duplicated Product'];
            return redirect()->back()->withErrors($error)->withInput();
        }

        $order = $request->all();

        $order['total'] = $total;

        $order = Order::create($order);

        $order->products()->attach($orderDetails);

        foreach ($products as $product) {
            $InventoryQuantity = DB::table('products')
                ->where('id', $product['product_id'])
                ->get('quantity')->first()->quantity;

            $editInvetoryQuantity = $InventoryQuantity - $product['quantity'];

            DB::table('products')->where('id', $product['product_id'])
                ->update(['quantity' => $editInvetoryQuantity]);
        }

        return redirect(route('admin.order.show', [$order]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        return view('admin.order.show', [
            'order' => $order,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        return view('admin.order.editForm', [
            'clients' => Client::get([
                'id',
                'name',
            ]),
            // 'products' => Product::get([
            //     'id',
            //     'name',
            //     'price',
            // ]),
            'products' => DB::table('products')->get(),
            // 'products' => $order->products,
            'order' => $order,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(OrderRequest $request, Order $order)
    {
        $oldProducts = $order->products;
        $request->merge([
            'user_id' => $request->user()->id,
        ]);
        $products = $request->products;

        $productIds = [];

        $total = 0;

        foreach ($products as $product) {
            $InventoryQuantity = DB::table('products')
                ->where('id', $product['product_id'])
                ->get('quantity')->first()->quantity;

            $productIds[] = $product['product_id'];

            $productPrice = $product['price'];

            $productQuantity = $product['quantity'];

            if ($InventoryQuantity < $productQuantity) {
                $error = ['invalid.quantity' => 'The inventory quantity of product ' . $product['product_id'] . ' is less than the ordered quantity'];
                return redirect()->back()->withErrors($error)->withInput();
            }

            $productTotalPrice = $productPrice * $productQuantity;

            $total += $productTotalPrice;
        }

        if (count($productIds) != count(array_unique($productIds))) {
            $error = ['products.duplicate' => 'duplicated Product'];
            return redirect()->back()->withErrors($error)->withInput();
        }

        $order['total'] = $total;

        $order->products()->sync($request->get('products'));

        $order->update($request->all());

        foreach ($oldProducts as $product) {

            $oldProductQuantity = $product->getOriginal('pivot_quantity');

            $newProductQuantity = DB::table('order_details')
                ->where(['order_id' => $order->id, 'product_id' => $product->getOriginal('id')])
                ->get('quantity')->first()->quantity;

            if ($newProductQuantity != $oldProductQuantity) {
                $quantityDifference = $newProductQuantity - $oldProductQuantity;

                $InventoryQuantity = DB::table('products')->where('id', $product->getOriginal('id'))->get('quantity')->first()->quantity;

                $editInvetoryQuantity = $InventoryQuantity - $quantityDifference;

                DB::table('products')->where('id', $product->getOriginal('id'))
                    ->update(['quantity' => $editInvetoryQuantity]);
            }
        }

        return redirect(route('admin.order.show', $order));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $order->delete();
        return redirect(route('admin.order.index'));
    }
}
