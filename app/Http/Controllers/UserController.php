<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        return view('admin.user.index');
    }

    public function show($userId)
    {
        $user = User::where('is_admin', 0)->get(['id', 'name', 'email'])->find($userId);
        if (!$user) {
            return back();
        } else {
            return view('admin.user.show', [
                'user' => $user,
            ]);
        }
    }

    public function create()
    {
        return view('admin.user.create');
    }

    public function store(Request $request)
    {

        $credintials = $request->validate([
            'name' => 'required|string',
            'email' => 'required|unique:users,email',
            'password' => 'required|confirmed|min:8',
        ]);
        
        $user = DB::table('users')->insert([
            'name' => $credintials['name'],
            'email' => $credintials['email'],
            'password' => Hash::make($credintials['password']),
            'is_admin' => 0,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        return redirect(route('admin.user.index'));
    }

    public function destroy(User $user)
    {
        $user->delete();
        return redirect(route('admin.user.index'));
    }
}
