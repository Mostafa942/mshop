<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model {
    use SoftDeletes;
    protected $fillable = [
        'date',
        'client_id',
        'user_id',
        'total',
    ];

    public function client() {
        return $this->belongsTo(Client::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function products() {
        return $this->belongsToMany(Product::class,
            'order_details',
            'order_id',
            'product_id')
            ->withPivot(['price', 'quantity', 'total_price'])
            ->withTimestamps()->withTrashed();
    }
}
